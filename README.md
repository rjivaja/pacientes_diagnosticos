Prueba técnica realizada por Raúl Jivaja Pérez para Mulhacén Soft, S.L.

El proyecto se basa en la necesidad de tener los datos sensibles de pacientes y diagnósticos encriptados en la BD por seguridad y privacidad.

Se ha realizado un CRUD de pacientes y otro CRUD de diagnósticos vinculado al de pacientes.

Se almacenan las versiones de cambios realizados cada vez que se edita un paciente o un diagnóstico.