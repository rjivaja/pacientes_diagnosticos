/**
 * Remove the specified resource from storage.
 *
 * @param {object|number} settings
 * @return {void}
 */
function destroy(settings) {

    let ini = {
        id: null,
        uri: null,
        message: 'Se va a borrar de forma definitiva el elemento seleccionado. ¿Continuar?',
        refresh: true,
        callback: undefined
    };

    if ('number' === typeof settings) {
        settings = $.extend(ini, {id: settings});
    } else {
        settings = $.extend(ini, settings);
    }

    if (!confirm(settings.message)) {
        return;
    }

    if (! settings.uri) {
        settings.uri = $('meta[name="uri"]').attr('content');
    }
    settings.uri = '/' + settings.uri.trimCharacter('/') + '/';

    $.ajax({
        method: 'DELETE',
        url: settings.uri + settings.id
    }).done(function (res) {
        if ('function' == typeof settings.callback){
            settings.callback(settings.id, res);
        }
        if (! settings.refresh) {
            return;
        }

        showAlert('#filter', 'success', 'Elemento borrado correctamente.');
        if ($('tr[data-id="' + settings.id + '"]').length) {
            $('tr[data-id="' + settings.id + '"]').remove();
        } else if ($('#datatable-filter').length) {
            $('#datatable-filter').DataTable().ajax.reload();
            $('#wrapper-modal > .modal').modal('hide');
        } else {
            window.location.reload();
        }
    });
}
