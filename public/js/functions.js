"use strict";

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

String.prototype.trimCharacter = function (c) {
    var target = this;
    if (c === "]") c = "\\]";
    if (c === "\\") c = "\\\\";
    return target.replace(new RegExp("^[" + c + "]+|[" + c + "]+$", "g"), "");
}

/**
 * Generate a URL friendly "slug" from a given string.
 *
 * @param  {string}  title
 * @param  {string}  separator
 * @return {string}
 */
function slugify(title, separator) {

    separator = separator || '-';

    title = title.replace(/^\s+|\s+$/g, ''); // trim
    title = title.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to = "aaaaeeeeiiiioooouuuunc------";

    for (var i = 0, l = from.length; i < l; i++) {
        title = title.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    title = title.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-') // collapse dashes
        .replace(/^-+/, '') // Trim - from start of title
        .replace(/-+$/, '') // Trim - from end of title

    if (separator != '-') {
        title = title.replaceAll('-', separator);
    }

    return title;
}

/**
 * Show alert.
 *
 * @param {string} selector
 * @param {string} type
 * @param {string} message
 * @return {void}
 */
function showAlert(selector, type, message) {
    hideAlert();
    var cs = $('.count_search').html() - 0;
    cs -= 1;
    $('.count_search').html(cs);
    var ct = $('.count_total').html() - 0;
    ct -= 1;
    $('.count_total').html(ct);

    var icon = '';
    switch (type) {
        case 'alert':
            icon = '<span class="fas fa-exclamation-circle fa-lg mr-2" aria-hidden="true"></span>';
            break;
        case 'warning':
            icon = '<span class="fas fa-info-circle fa-lg mr-2" aria-hidden="true"></span>';
            break;
        case 'success':
            icon = '<span class="fas fa-check-circle fa-lg mr-2" aria-hidden="true"></span>';
            break;
        case 'info':
            icon = '<span class="fas fa-info-circle fa-lg mr-2" aria-hidden="true"></span>';
            break;
    }

    var html = '<div id="notify" class="row"><div class="col-12"><div class="alert alert-' + type + ' alert-dismissible fade show text-center" role="alert">';
    html += icon + message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    html += '</div></div></div>';

    $(selector).prepend(html);
}

/**
 * Hide alert.
 *
 * @return {void}
 */
function hideAlert() {
    $('#notify').fadeOut();
}

/**
 * Show loading effect
 *
 * @param  {string} selector
 * @return {void}
 */
function loading(selector) {
    if (false === selector) {
        $('body').removeClass('overlay');
    } else if (selector) {
        if ($(selector + ' #loading').length) {
            $(selector + ' #loading').hide();
            $(selector + ' #loading').remove();
        } else {
            var loading = $('#loading').html();
            $(selector).css('position', 'relative');
            $(selector).append('<div id="loading" style="position: absolute;">' + loading + '</div>');
            $(selector + ' #loading').show();
        }
    } else {
        $('body').toggleClass('overlay', !$('body').hasClass('overlay'));
    }
}

/**
 * Active confirm exit dialog.
 *
 * @return {void}
 */
function confirmExitDialog() {
    $(this).closest('form').data('changed', true);

    if (!_confirmExitDialog) {
        return;
    }

    window.onbeforeunload = function (e) {
        return true;
    };
}

/**
 * Check if confirm exit dialog is necessary.
 *
 * @return {void}
 */
function confirmExitDialogCheck() {
    $('form [type="submit"]').on('click', function () {
        var flag = true;
        if ($(this).closest('form').data('changed')) {
            $(this).closest('form').find(':input[required]').each(function (index, item) {
                if (null == $(item).val() || !$(item).val().length) {
                    flag = false;
                }
            });
            if (flag) {
                loading();
                window.onbeforeunload = function (e) {
                    return;
                };
            }
        } else {
            $(this).closest('form').find(':input[required]').each(function (index, item) {
                if (null == $(item).val() || !$(item).val().length) {
                    flag = false;
                }
            });
            if (flag) {
                loading();
            }
        }
    });
}

function modalAjax(url, params, callback) {
    // url = ER_URL + url;
    // params.json = true;

    // loading();
    $.post(url, params, function (res) {
        if (res.status) {
            if ($('.modal-backdrop').length) {
                $('#wrapper-modal > .modal').modal('hide');
                $('#wrapper-modal > .modal').on('hidden.bs.modal', function (e) {
                    $('#wrapper-modal').html(res.view);
                    $('#wrapper-modal > .modal').modal('show');
                    if (typeof callback == 'function') {
                        callback(res.data);
                    }
                });
            } else {
                $('#wrapper-modal').html(res.view);
                $('#wrapper-modal > .modal').modal('show');
                if (typeof callback == 'function') {
                    callback(res.data);
                }
            }
        } else {
            $('#wrapper-modal').html(res);
            $('#wrapper-modal > .modal').modal('show');
            if (typeof callback == 'function') {
                callback(res.data);
            }
        }
    }).always(function () {
        // loading(false);
    });
}

/**
 * Convert json to html name for input
 *
 * @param {json} json
 * @param {array} result
 * @param {string} parent
 */
function json2html_name_list(json, result, parent) {
    if (!result) {
        result = {};
    }

    if (!parent) {
        parent = '';
    }

    if ('object' != (typeof json)) {
        result[parent] = json;
    } else {
        for (var key in json) {
            var value = json[key];
            if ('' == parent) {
                var subparent = key;
            } else {
                var subparent = parent + '[' + key + ']';
            }
            result = json2html_name_list(value, result, subparent);
        }
    }

    return result;
}

function checkBrowserSync() {
    if (-1 !== window.location.host.indexOf('localhost') && 3000 == window.location.port) {
        browserSyncActive = true;
    }
}

function formChangeFormat($form, original) {
    original = original || false;

    const items = [
        {
            selector: '.datetime,[data-datepicker="datetime"]',
            formats: {
                in: 'DD/MM/YYYY HH:mm',
                out: 'YYYY-MM-DD HH:mm'
            }
        }, {
            selector: '.date,[data-datepicker="date"]',
            formats: {
                in: 'DD/MM/YYYY',
                out: 'YYYY-MM-DD'
            }
        }
    ];

    $(items).each(function (index, element) {
        $form.find(element.selector).each(function (index, item) {
            if ($(item).val().length) {
                const _in = original ? element.formats.out : element.formats.in;
                const _out = original ? element.formats.in : element.formats.out;
                $(item).val(moment($(item).val(), _in).format(_out));
            }
        });
    });
}

$(function () {

    checkBrowserSync();
    moment.locale('es');
    numeral.locale('es-es');

    $.extend(true, $.fn.datetimepicker.defaults, {
        locale: 'es',
        icons: {
            time: 'far fa-clock',
            date: 'far fa-calendar',
            up: 'fas fa-arrow-up',
            down: 'fas fa-arrow-down',
            previous: 'fas fa-chevron-left',
            next: 'fas fa-chevron-right',
            today: 'far fa-calendar-check',
            clear: 'fas fa-trash',
            close: 'fas fa-times'
        }
    });



    $('form').on('submit', function (e) {
        // e.preventDefault();

        const $form = $(this);
        formChangeFormat($form);

        // if ($form.has('data-ajax').length) {
        //     let params = {};
        //     $form.serializeArray().map(function(item) {
        //         params[item.name] = item.value;
        //     });

        //     $.ajax({
        //         method: $form.attr('method'),
        //         url: $form.attr('action'),
        //         data: params
        //     }).fail(function (res) {
        //         formChangeFormat($form, true);
        //     });

        //     return false;
        // } else {
            $form.on('submit', function (e) {
                formChangeFormat($form, true);
            });
        // }

        return true;
    });

    $(':input').on('change', function (e) {
        $(this).closest('form').find('[data-required]').each(function (index, elem) {
            var selector = $(elem).data('required');
            var value = $(elem).data('required-value') - 0;
            var condition = $(elem).data('required-condition') || '==';
            var id = $(selector).val() - 0;

            if ('!=' == condition) {
                $(elem).find('label small').toggle((id != value));
                if (id != value) {
                    $(elem).find(':input').attr('required');
                } else {
                    $(elem).find(':input').removeAttr('required');
                }
            } else {
                $(elem).find('label small').toggle((id == value));
                if (id == condition) {
                    $(elem).find(':input').attr('required');
                } else {
                    $(elem).find(':input').removeAttr('required');
                }
            }
        });
    }).trigger('change');

    // Reset data form
    $('button[type="reset"]').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).closest('form').find('select').val(null).trigger('change');
        $(this).closest('form').find('select > option:selected').prop('selected', false).trigger('change');
        $(this).closest('form').find('input:not([name="_token"])').val('');

        return false;
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ajaxSend(function (event, request, settings) {
        if (! settings.url.includes('datatable') ) {
            loading();
        }
    }).ajaxComplete(function() {
        setTimeout(function () {
            $('body').removeClass('overlay');
        }, 250);
    }).ajaxError(function (event, jqXHR, ajaxSettings, thrownError) {
        let selector = $('.modal.show').length ? '.modal.show .modal-body' : '#filter';

        if (!$(selector).length) {
            selector = $(event.target.activeElement).closest('.table-responsive').first();
        }

        if (405 == jqXHR.status) {
            showAlert(selector, 'danger', 'No tienes permiso para realizar esta acción.');
        } else if (422 == jqXHR.status) {
            if (jqXHR.responseJSON.errors) {
                let error = [];
                $.each(jqXHR.responseJSON.errors, function (index, element) {
                    if ('object' == typeof element) {
                        element = element.join('<br>');
                    }
                    error.push(element);
                });
                showAlert(selector, 'danger', error.join('<br>'));
            } else {
                showAlert(selector, 'danger', jqXHR.responseJSON.error);
            }
        } else {
            showAlert(selector, 'danger', 'Se ha producido un error. Revisa los datos y vuelve a intentarlo.');
        }
    });
});

