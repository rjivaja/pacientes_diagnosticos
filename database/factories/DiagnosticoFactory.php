<?php

namespace Database\Factories;

use App\Models\Diagnostico;
use Illuminate\Database\Eloquent\Factories\Factory;

class DiagnosticoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Diagnostico::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {


        return [
            'paciente_id' => $this->faker->numberBetween(1, 100),
            'descripcion' => $this->faker->text(),
            'fecha' => $this->faker->dateTime(),
        ];
    }
}
