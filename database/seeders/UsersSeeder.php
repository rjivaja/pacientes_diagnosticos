<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'test@test.com',
            'email_verified_at' => now(),
            'password' => bcrypt('jivaja'),
            'remember_token' => Str::random(10),
            'name' => 'Usuario de prueba'
        ]);
    }
}
