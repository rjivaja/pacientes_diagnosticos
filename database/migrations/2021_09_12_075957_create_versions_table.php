<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        Para las versiones, se opta por una tabla polimórfica en la que almacenamos los siguientes datos:
        - fecha: fecha de la version
        - model: Modelo que queremos versionar (al ser campo morphs - se crean "model_type" y "model_id" a nivel de BD)
        - data: datos en formato json asociativo

        De este modo, al usar la tabla polimórfica y los datos en formato json podemos usarlo en este caso
         para Pacientes y Diagnósticos, pero se puede usar a futuro para versionar cualquier modelo
        */

        Schema::create('versions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->dateTime('fecha');

            $table->morphs('model');
            $table->text('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('versions');
    }
}
