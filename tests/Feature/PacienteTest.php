<?php

namespace Tests\Feature;

use App\Models\Paciente;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PacienteTest extends TestCase
{
    /**
     * Test de creación de paciente
     *
     * @return void
     */
    public function test_create_paciente()
    {
        $user = User::find(1);

        $response = $this->actingAs($user)->post('/pacientes', [
            'nombre_apellidos' => 'Raul Jivaja',
            'dni' => '14631176W'
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('pacientes');

        $paciente = Paciente::latest()->first();

        $this->assertEquals('Raul Jivaja', $paciente->nombre_apellidos);
        $this->assertEquals('14631176W', $paciente->dni);
    }
}
