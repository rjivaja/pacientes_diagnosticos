<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Diagnostico extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Userstamps;

    protected $fillable = [
        'paciente_id',
        'descripcion',
        'fecha',
    ];

    /**
     * Encriptamos las cadenas sensibles
     *
     * @var array
     */
    protected $casts = [
        'descripcion' => 'encrypted',
    ];

    /**
     * Obtener todas las versiones de datos de un diagnostico
     */
    public function versiones()
    {
        return $this->morphMany(Version::class, 'versiones', 'model_type', 'model_id');
    }
}
