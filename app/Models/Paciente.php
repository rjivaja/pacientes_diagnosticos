<?php

namespace App\Models;

use App\Models\Paciente as ModelsPaciente;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Paciente extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Userstamps;

    protected $fillable = [
        'nombre_apellidos',
        'dni',
    ];

    /**
     * Encriptamos las cadenas sensibles
     *
     * @var array
     */
    protected $casts = [
        'nombre_apellidos' => 'encrypted',
        'dni' => 'encrypted',
    ];

    /**
     * Obtener todos los diagnósticos de un paciente
     */
    public function diagnosticos()
    {
        return $this->hasMany(Diagnostico::class);
    }

    /**
     * Obtener todas las versiones de datos de un paciente
     */
    public function versiones()
    {
        return $this->morphMany(Version::class, 'versiones', 'model_type', 'model_id');
    }

    /**
     * Scope a query to filter.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  array  $data
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter(Builder $query, array $data)
    {
        if (!empty($search = $data['search'] ?? null)) {
            $query->where(function ($query) use ($search) {
                $query->where('nombre_apellidos', 'like', "%{$search}%")
                    ->orWhere('dni', 'like', "%{$search}%");
            });
        } elseif (!empty($search = $data['id'] ?? null)) {
            $query->where('id', $search);
        }

        return $query;
    }
}
