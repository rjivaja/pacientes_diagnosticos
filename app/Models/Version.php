<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'fecha',
        'model_type',
        'model_id',
        'data',
    ];
}
