<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiagnosticosStoreRequest;
use App\Models\Diagnostico;
use App\Models\Paciente;
use App\Models\Version;
use Illuminate\Http\Request;

class DiagnosticoController extends Controller
{
    /**
     * Mostrar el formulario de creación de diagnostico pasándole el usuario al que se le asigna
     *
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function create(Paciente $paciente)
    {
        // autocompletamos la fecha con el dia de hoy
        $fecha = date('d/m/Y');
        return view('diagnostico.create', compact('paciente', 'fecha'));
    }

    /**
     * Guardar la información insertada del diagnostico asociado al paciente
     *
     * @param  \App\Http\Requests\DiagnosticosStoreRequest  $request
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function store(DiagnosticosStoreRequest $request, Paciente $paciente)
    {
        $data = $request->validated();

        // asignamos el id del paciente
        $data['paciente_id'] = $paciente->id;

        Diagnostico::create($data);

        session()->flash('alert-success', 'El diagnóstico se ha creado correctamente.');
        return redirect()->route('pacientes.show', $paciente->id);
    }

    /**
     * Mostrar el formulario de edición de los datos del diagnostico
     *
     * @param  \App\Models\Diagnostico  $diagnostico
     * @return \Illuminate\Http\Response
     */
    public function edit(Diagnostico $diagnostico)
    {
        if (!empty(old('fecha'))) {
            $fecha = old('fecha');
        } else {
            $fecha = $diagnostico->fecha;
        }
        return view('diagnostico.edit', compact('diagnostico', 'fecha'));
    }

    /**
     * Actualizar la información de los datos del diagnostico
     *
     * @param  \App\Http\Requests\DiagnosticoUpdateRequest  $request
     * @param  \App\Models\Diagnostico  $diagnostico
     * @return \Illuminate\Http\Response
     */
    public function update(DiagnosticosStoreRequest $request, Diagnostico $diagnostico)
    {
        $data = $request->validated();

        // almacenamos la info anterior como version
        $data_version['fecha'] = date('Y-m-d H:i:s');
        $data_version['model_type'] = Diagnostico::class;
        $data_version['model_id'] = $diagnostico->id;
        $data_version['data'] = json_encode($diagnostico->getAttributes());

        Version::create($data_version);

        // almacenamos la info nueva
        $diagnostico->fill($data);
        $diagnostico->save();

        session()->flash('alert-success', 'Se ha editado el diagnostico correctamente.');
        return redirect()->route('pacientes.show', $diagnostico->paciente_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Diagnostico  $diagnostico
     * @return \Illuminate\Http\Response
     */
    public function destroy(Diagnostico $diagnostico)
    {
        if ($diagnostico->delete()) {
            return response()->json(['message' => 'ok'], 202);
        }

        return response()->json(['error' => 'Internal Server Error'], 500);
    }
}
