<?php

namespace App\Http\Controllers;

use App\Models\Diagnostico;
use App\Models\Paciente;
use App\Models\Version;
use Illuminate\Http\Request;

class VersionController extends Controller
{
    /**
     * Mostramos el listado con las versiones de datos del paciente
     *
     * @param \App\Models\Paciente
     * @return \Illuminate\Http\Response
     */
    public function versionesPaciente(Paciente $paciente)
    {
        return view('version.paciente', compact('paciente'));
    }

    /**
     * Mostramos el listado con las versiones de datos del diagnóstico
     *
     * @param \App\Models\Diagnostico
     * @return \Illuminate\Http\Response
     */
    public function versionesDiagnostico(Diagnostico $diagnostico)
    {
        return view('version.diagnostico', compact('diagnostico'));
    }
}
