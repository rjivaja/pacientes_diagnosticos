<?php

namespace App\Http\Controllers;

use App\Http\Requests\PacienteSearchRequest;
use App\Http\Requests\PacientesStoreRequest;
use App\Http\Requests\PacienteUpdateRequest;
use App\Models\Diagnostico;
use App\Models\Paciente;
use App\Models\Version;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    /**
     * Mostrar el listado filtrable de pacientes
     *
     * @param  \App\Http\Requests\PacienteSearchRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function index(PacienteSearchRequest $request)
    {
        $pacientes = Paciente::all();

        return view('paciente.index', compact('pacientes'));
    }

    /**
     * Mostrar el formulario de creación de paciente
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paciente.create');
    }

    /**
     * Guardar la información insertada del paciente
     *
     * @param  \App\Http\Requests\PacientesStoreRequest;  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PacientesStoreRequest $request)
    {
        $data = $request->validated();
        Paciente::create($data);

        session()->flash('alert-success', 'El paciente se ha creado correctamente.');
        return redirect()->route('pacientes.index');
    }

    /**
     * Mostrar la info de un paciente junto con los diagnósticos del mismo
     *
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function show(Paciente $paciente)
    {
        // cargamos los diagnosticos del paciente ordenados por fecha descendente (más recientes primero)
        $diagnosticos = $paciente->diagnosticos()->orderBy('fecha', 'desc')->get();

        if ($paciente) {
            return view('paciente.show', compact('paciente', 'diagnosticos'));
        } else {
            session()->flash('alert-error', 'El paciente seleccionado no existe.');
            return redirect()->route('pacientes.index');
        }
    }

    /**
     * Mostrar el formulario de edición de los datos del paciente
     *
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente)
    {
        return view('paciente.edit', compact('paciente'));
    }

    /**
     * Actualizar la información de los datos del paciente
     *
     * @param  \App\Http\Requests\PacienteUpdateRequest  $request
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function update(PacienteUpdateRequest $request, Paciente $paciente)
    {
        $data = $request->validated();
        // almacenamos la info anterior como version
        $data_version['fecha'] = date('Y-m-d H:i:s');
        $data_version['model_type'] = Paciente::class;
        $data_version['model_id'] = $paciente->id;
        $data_version['data'] = json_encode($paciente->getAttributes());

        Version::create($data_version);

        // almacenamos la info nueva
        $paciente->fill($data);
        $paciente->save();

        session()->flash('alert-success', 'Se ha editado el paciente correctamente.');
        return redirect()->route('pacientes.show', $paciente->id);
    }

    /**
     * Borrar de forma lógica (con softdelete) el paciente seleccionado
     *
     * @param  \App\Models\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente)
    {
        // Al tener en la estructura de la tabla diagnostico "CascadeOnDelete"
        // no es necesario borrar los diagnosticos del paciente ya que lo hace de forma interna

        if ($paciente->delete()) {
            return response()->json(['message' => 'ok'], 202);
        }

        return response()->json(['error' => 'Internal Server Error'], 500);
    }
}
