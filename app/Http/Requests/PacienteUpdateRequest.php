<?php

namespace App\Http\Requests;

use App\Rules\nifnie;
use Illuminate\Foundation\Http\FormRequest;

class PacienteUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Definimos las reglas de validacion del request
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_apellidos' => 'required|string|max:250',
            'dni' => [
                'required',
                'string',
                new nifnie(),
                'unique:App\Models\Paciente,dni,' . $this->paciente->id . ',id,deleted_at,NULL'
            ]
        ];
    }

    /**
     * Personalizamos algunos mensajes de error para la validacion
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre_apellidos.required' => 'Nombre completo: Obligatorio',
            'nombre_apellidos.max' => 'Nombre completo: Longitud máxima 250 caracteres',
            'dni.required' => 'DNI: Obligatorio',
            'dni.unique' => 'Existe otro paciente con el mismo DNI',
        ];
    }
}
