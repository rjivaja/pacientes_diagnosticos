<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PacienteSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Definimos las reglas de validacion del request
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search' => 'nullable|string',
        ];
    }

    /**
     * Almacenamos las cadenas de atributos para validaciones.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'search' => 'buscador global',
        ];
    }
}
