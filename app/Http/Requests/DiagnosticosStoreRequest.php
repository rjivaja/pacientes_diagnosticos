<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiagnosticosStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion' => 'required|string|max:500',
            'fecha' => 'required|date'
        ];
    }

    /**
     * Personalizamos algunos mensajes de error para la validacion
     *
     * @return array
     */
    public function messages()
    {
        return [
            'descripcion.required' => 'Descripción: Obligatorio',
            'descripcion.max' => 'Descripción: Longitud máxima 500 caracteres',
            'fecha.required' => 'Fecha: Obligatorio',
            'fecha.date' => 'Fecha: Debe ser una fecha en formato válido dd/mm/aaaa',
        ];
    }
}
