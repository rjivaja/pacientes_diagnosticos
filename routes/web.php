<?php

use App\Http\Controllers\DiagnosticoController;
use App\Http\Controllers\PacienteController;
use App\Http\Controllers\VersionController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__ . '/auth.php';

Route::middleware('auth')->group(function () {
    // Listado de pacientes
    Route::get('/', [PacienteController::class, 'index'])->name('pacientes.index');

    // CRUD de pacientes
    Route::resource('pacientes', PacienteController::class);

    Route::group([
        'prefix' => 'pacientes/{paciente}',
        'as' => 'pacientes.',
    ], function () {
        // Diagnosticos de paciente
        Route::resource('diagnosticos', DiagnosticoController::class)->only('store', 'create');

        // Versiones de datos de paciente
        Route::get('versiones', [VersionController::class, 'versionesPaciente'])->name('versiones');
    });

    // Diagnosticos (show, edit, update, destroy)
    Route::resource('diagnosticos', DiagnosticoController::class)->except('create', 'store', 'index');

    Route::group([
        'prefix' => 'diagnosticos/{diagnostico}',
        'as' => 'diagnosticos.',
    ], function () {
        // Versiones de datos de diagnosticos
        Route::get('versiones', [VersionController::class, 'versionesDiagnostico'])->name('versiones');
    });
});
