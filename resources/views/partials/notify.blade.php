@if ($errors->any())
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger fade show" role="alert">
                <div class="row">
                    <div class="col-auto pr-0">
                        <span class="fas fa-exclamation-circle fa-lg mr-2" aria-hidden="true"></span>
                    </div>
                    <div class="col pl-0">
                        <strong>Se han detectado los siguientes errores: </strong><br>
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
