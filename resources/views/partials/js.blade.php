<script>
    var browserSyncActive = false;

    $(function () {
        $('.btn-back').click(function (e) {
            var url = '{{ url()->previous() }}';
            if (! url.length || url == window.location.href || ! url.includes('{{ url('/') }}')) {
                url = '{{ url('/') }}\/' + $('meta[name="uri"]').attr('content');
            }

            window.location.href = url;
        });
    });
</script>
