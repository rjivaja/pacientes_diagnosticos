@php $path = Request::path() @endphp

<!-- sidebar-->
<aside class="aside-container">
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
        <nav class="sidebar" data-sidebar-anyclick-close="">
            <!-- START sidebar nav-->
            <ul class="sidebar-nav">
                <li class="has-user-block">
                    <div id="user-block">
                        <div class="item user-block">
                            <!-- Name and Job-->
                            <div class="user-block-info">
                                <span class="user-block-name">Hola, {{ Auth::user()->name }}</span>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="{{ request()->is('/') || request()->is('pacientes*') ? 'active' : '' }}">
                    <a href="{{ route('pacientes.index') }}" title="Pacientes">
                        <em class="fas fa-users fa-fw"></em>
                        <span>Pacientes</span>
                    </a>
                </li>

                {{-- <li class="{{ request()->is('diagnosticos*') ? 'active' : '' }}">
                    <a href="{{ route('diagnosticos.index') }}" title="Usuarios">
                        <em class="fas fa-users fa-fw"></em>
                        <span>Diagnosticos</span>
                    </a>
                </li> --}}
            </ul>
            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>
