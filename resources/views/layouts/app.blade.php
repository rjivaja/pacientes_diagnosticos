<!DOCTYPE html>
<html lang="es" class="h-100">

@php @list($ctrl, $action) = explode('@', Route::currentRouteAction()); @endphp

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="uri" content="{{ str_replace('/search', '', request()->path()) }}">
    <meta name="base" content="{{ URL::to('/') }}">

    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}?v={{ env('APP_VERSION') }}">
    <title>@yield('title', 'Inicio') | {{ env('APP_NAME') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" id="bscss">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" id="maincss">
    <link rel="stylesheet" href="{{ asset('css/backend.css') }}" id="backendcss">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />

    @yield('style')

</head>

<body id="body" class="layout-fixed {{ Request::segment(1) }} {{ str_replace('.', '-', Route::currentRouteName()) }} {{ $action }} @yield('class')">
    <div class="wrapper">

        @include('partials.navbar')

        @include('partials.sidebar')

        <!-- End-Page filter -->
        @yield('page-filter')
        <!-- Page container -->

        <!-- Main section-->
        <section class="section-container">
            <!-- Page content-->
            <div class="content-wrapper">
                <div class="content-heading">
                    <div class="row">
                        <div class="col-auto">
                            <div>@yield('title')</div>
                        </div>
                        <div class="col d-flex align-items-end justify-content-end">
                            @yield('buttons')
                        </div>
                    </div>
                </div>

                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <div role="alert" class="alert alert-{{ $msg }} alert-dismissible fade show text-center">
                            @if ($msg == 'danger')
                                <span class="fas fa-exclamation-circle fa-lg mr-2" aria-hidden="true"></span>
                            @elseif($msg == 'warning')
                                <span class="fas fa-info-circle fa-lg mr-2" aria-hidden="true"></span>
                            @elseif($msg == 'success')
                                <span class="fas fa-check-circle fa-lg mr-2" aria-hidden="true"></span>
                            @elseif($msg == 'info')
                                <span class="fas fa-info-circle fa-lg mr-2" aria-hidden="true"></span>
                            @endif
                            <span class="sr-only">{{ $msg }}:</span>
                            {{ Session::get('alert-' . $msg) }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                @endforeach

                @include('partials.notify')

                @yield('content')
            </div>
        </section>

        <footer class="footer-container">
            <span>&copy; <?= @date('Y'); ?> | Raúl Jivaja Pérez</span>
        </footer>
    </div> <!-- wrapper -->

    <!-- Wrapper modal -->
    <div id="wrapper-modal"></div>
    <!-- /wrapper modal -->

    <div class="loading">
        <img src="{{ asset('svg/loading.svg') }}" alt="" srcset="">
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js" integrity="sha512-ubuT8Z88WxezgSqf3RLuNi5lmjstiJcyezx34yIU2gAHonIi27Na7atqzUZCOoY4CExaoFumzOsFQ2Ch+I/HCw==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js" integrity="sha512-XKa9Hemdy1Ui3KSGgJdgMyYlUg1gM+QhL6cnlyTe2qzMCYm4nAZ1PsVerQzTTXzonUR+dmswHqgJPuwCq1MaAg==" crossorigin="anonymous"></script>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/functions.js') }}?v={{ config('app.version') }}"></script>
    <script src="{{ asset('js/controller.js') }}?v={{ config('app.version') }}"></script>

    @include('partials.js')

    @yield('script')
</body>
</html>
