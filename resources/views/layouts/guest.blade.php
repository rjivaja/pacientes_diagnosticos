<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}?v={{ env('APP_VERSION') }}">
    <title>@yield('title', 'Inicio') | {{ env('APP_NAME') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" id="bscss">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" id="maincss">
    <link rel="stylesheet" href="{{ asset('css/backend.css') }}" id="backendcss">

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />

    @yield('style')

</head>

<body class="basic">
   <div class="wrapper">

        <section>
            <!-- Page content-->
            <div>

                @yield('content')

            </div>
        </section>

        <footer class="footer-container ml-0">
            <span>&copy; <?= @date('Y'); ?> | Raúl Jivaja Pérez</span>
        </footer>

    </div> <!-- wrapper -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js" integrity="sha512-ubuT8Z88WxezgSqf3RLuNi5lmjstiJcyezx34yIU2gAHonIi27Na7atqzUZCOoY4CExaoFumzOsFQ2Ch+I/HCw==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js" integrity="sha512-XKa9Hemdy1Ui3KSGgJdgMyYlUg1gM+QhL6cnlyTe2qzMCYm4nAZ1PsVerQzTTXzonUR+dmswHqgJPuwCq1MaAg==" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/js-storage@1.1.0/js.storage.min.js" integrity="sha256-6Okzq6GEjXvNVQkHaonBRB/rChSIvMZn037l+lcH4Q4=" crossorigin="anonymous"></script>

    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        $(function () {
            $(window).resize(function () {
                if ($('body').height() < $('body > .wrapper').height()) {
                    $('html, body').css('height', 'auto');
                } else {
                    $('html, body').css('height', '100%');
                }
            }).trigger('resize');

            $('.toggle-password').click(function (e) {
                $(this).prev('input').attr('type', 'text');
            });
        });
    </script>

    @include('partials.js')

    @yield('script')
</body>

</html>
