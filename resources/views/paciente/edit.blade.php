@extends('layouts.app')

@section('title', 'Editar paciente')

@section('content')

    {!! Form::model($paciente, ['route' => ['pacientes.update', $paciente->id], 'method' => 'PUT']) !!}
        @include('paciente.fields')

        <div class="row">
            <div class="col-xl-8 col-xxl-6 text-right">
                <button class="btn btn-secondary btn-back mr-2" type="button"><i class="fas fa-times pr-2"></i> Cancelar</button>
                <button class="btn btn-primary" type="submit"><i class="fas fa-save pr-2"></i> Editar paciente</button>
            </div>
        </div>
    {!! Form::close() !!}

@endsection
