@extends('layouts.app')

@section('title', 'Gestión de pacientes')

@section('buttons')

    <ul class="list-inline list-unstyled mb-0">
        <li class="list-inline-item">
            <a class="btn btn-primary" href="{{ route('pacientes.create') }}">
                <i class="fas fa-plus pr-2"></i>Nuevo paciente
            </a>
        </li>
    </ul>

@endsection

@section('content')

    <div class="card card-default">
        <div class="card-body">
            <div id="filter" class="main-content">
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <colgroup>
                                    <col width="50">
                                    <col>
                                    <col width="150">
                                    <col width="150">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre completo</th>
                                        <th>DNI</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($pacientes as $paciente)
                                        <tr data-id="{{ $paciente->id }}">
                                            <td><a href="{{ route('pacientes.show', $paciente->id) }}">{{ $paciente->id }}</a></td>
                                            <td><a href="{{ route('pacientes.show', $paciente->id) }}">{{ $paciente->nombre_apellidos }}</a></td>
                                            <td><a href="{{ route('pacientes.show', $paciente->id) }}">{{ $paciente->dni }}</a></td>
                                            <td>
                                                <a href="{{ route('pacientes.show', $paciente->id) }}" title="Ver ficha de paciente"><i class="mr-2 fas fa-lg fa-eye"></i></a>
                                                <a href="{{ route('pacientes.versiones', $paciente->id) }}" title="Consultar histórico de versiones"><i class="mr-2 fas fa-lg fa-history"></i></a>
                                                <a href="{{ route('pacientes.edit', $paciente->id) }}" title="Editar paciente"><i class="mr-2 fas fa-lg fa-pencil-alt"></i></a>
                                                <a onclick="destroy({uri: 'pacientes', id: {{ $paciente->id }} });return false;" href="#" title="Borrar paciente"><i class="fas fa-trash fa-lg"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

