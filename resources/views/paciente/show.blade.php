@extends('layouts.app')

@section('title', 'Ficha de paciente')

@section('content')
    {!! Form::model($paciente) !!}
    <div class="row">
        <div class="col-xl-8 col-xxl-6">
            <div class="card card-default">
                <div class="card-header">
                    Datos personales
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9 form-group">
                            <label>Nombre completo</label>
                            {!! Form::text('nombre_apellidos', null, ['class' => 'form-control', 'readonly']) !!}
                        </div>
                        <div class="col-md-3 form-group">
                            <label>DNI</label>
                            {!! Form::text('dni', null, ['class' => 'form-control', 'readonly']) !!}
                        </div>
                        <div class="col-md-12">
                            <p class="text-right">
                                <a class="btn btn-warning mr-2" href="{{ route('pacientes.versiones', $paciente->id) }}">
                                    <i class="fas fa-history pr-2 "></i>Versiones de datos
                                </a>
                                <a class="btn btn-primary" href="{{ route('pacientes.edit', $paciente->id) }}">
                                    <i class="fas fa-pencil-alt pr-2"></i>Editar paciente
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <div class="row">
        <div class="col-xl-8 col-xxl-6">
            <div class="card card-default">
                <div class="card-header">
                    Diagnósticos del paciente
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if($paciente->diagnosticos->isEmpty())
                                <p class="alert alert-warning text-center">No existen diagnósticos del paciente</p>
                            @else
                                <div class="table-responsive">
                                    <p class="text-right">
                                        <a class="btn btn-primary" href="{{ route('pacientes.diagnosticos.create', $paciente) }}">
                                            <i class="fas fa-plus pr-2"></i>Añadir diagnostico
                                        </a>
                                    </p>

                                    <table class="table table-hover table-striped">
                                        <colgroup>
                                            <col width="50">
                                            <col>
                                            <col width="100">
                                            <col width="105">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Descripción</th>
                                                <th>Fecha</th>
                                                <th class="text-center">Opciones</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($diagnosticos as $diagnostico)
                                                <tr data-id="{{ $diagnostico->id }}">
                                                    <td>{{ $diagnostico->id }}</td>
                                                    <td>{{ $diagnostico->descripcion }}</td>
                                                    <td>{{ date('d/m/Y', strtotime($diagnostico->fecha)) }}</td>
                                                    <td>
                                                        <a title="Consultar histórico de versiones" href="{{ route('diagnosticos.versiones', $diagnostico->id) }}"><i class="mr-2 fas fa-lg fa-history"></i></a>
                                                        <a title="Editar diagnóstico" href="{{ route('diagnosticos.edit', $diagnostico->id) }}"><i class="mr-2 fas fa-lg fa-pencil-alt"></i></a>
                                                        <a onclick="destroy({uri: 'diagnosticos', id: {{ $diagnostico->id }} });return false;" href="#" title="Borrar diagnóstico"><i class="fas fa-trash fa-lg"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
