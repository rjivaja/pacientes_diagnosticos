<div class="row">
    <div class="col-xl-8 col-xxl-6">
        <div class="card card-default">
            <div class="card-header">
                Datos personales
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-9 form-group">
                        <label>Nombre completo<small class="text-danger">(*)</small></label>
                        {!! Form::text('nombre_apellidos', null, ['class' => 'form-control', 'required']) !!}
                    </div>
                    <div class="col-md-3 form-group">
                        <label>DNI <small class="text-danger">(*)</small></label>
                        {!! Form::text('dni', null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
