@extends('layouts.app')

@section('title', 'Versiones de datos de paciente')

@section('content')
    {!! Form::model($paciente) !!}
    <div class="row">
        <div class="col-xl-8 col-xxl-6">
            <div class="card card-default">
                <div class="card-header">
                    Datos actuales del paciente
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9 form-group">
                            <label>Nombre completo</label>
                            {!! Form::text('nombre_apellidos', null, ['class' => 'form-control', 'readonly']) !!}
                        </div>
                        <div class="col-md-3 form-group">
                            <label>DNI</label>
                            {!! Form::text('dni', null, ['class' => 'form-control', 'readonly']) !!}
                        </div>
                        <div class="col-md-12">
                            <p class="text-right">
                                <a class="btn btn-warning" href="{{ route('pacientes.show', $paciente->id) }}">
                                    <i class="fas fa-arrow-left pr-2"></i>Volver a la ficha del paciente
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <div class="row">
        <div class="col-xl-8 col-xxl-6">
            <div class="card card-default">
                <div class="card-header">
                    Versiones de datos
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if($paciente->versiones->isEmpty())
                                <p class="alert alert-warning text-center">No existen versiones de datos del paciente</p>
                            @else
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped">
                                        <colgroup>
                                            <col width="50">
                                            <col width="100">
                                            <col>
                                            <col width="105">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Fecha V.</th>
                                                <th>Nombre completo</th>
                                                <th>DNI</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($paciente->versiones as $version)
                                                @php
                                                    $data_version = json_decode($version->data);
                                                @endphp

                                                <tr data-id="{{ $version->id }}">
                                                    <td>{{ $version->id }}</td>
                                                    <td>{{ date('d/m/Y H:i', strtotime($version->fecha)) }}</td>
                                                    <td>{{ Crypt::decryptString($data_version->nombre_apellidos) }}</td>
                                                    <td>{{ Crypt::decryptString($data_version->dni) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
