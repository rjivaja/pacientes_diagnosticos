@extends('layouts.app')

@section('title', 'Versiones de datos de diagnóstico')

@section('content')
    {!! Form::model($diagnostico) !!}
    <div class="row">
        <div class="col-xl-8 col-xxl-6">
            <div class="card card-default">
                <div class="card-header">
                    Datos actuales del diagnóstico
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>Fecha</label>
                            {!! Form::text('fecha', null, ['class' => 'form-control', 'readonly']) !!}
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Descripción</label>
                            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'readonly']) !!}
                        </div>
                        <div class="col-md-12">
                            <p class="text-right">
                                <a class="btn btn-warning" href="{{ route('pacientes.show', $diagnostico->paciente_id) }}">
                                    <i class="fas fa-arrow-left pr-2"></i>Volver a la ficha del paciente
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <div class="row">
        <div class="col-xl-8 col-xxl-6">
            <div class="card card-default">
                <div class="card-header">
                    Versiones de datos
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if($diagnostico->versiones->isEmpty())
                                <p class="alert alert-warning text-center">No existen versiones de datos del diagnóstico</p>
                            @else
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped">
                                        <colgroup>
                                            <col width="50">
                                            <col width="100">
                                            <col width="100">
                                            <col>
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Fecha V.</th>
                                                <th>Fecha D.</th>
                                                <th>Descripción</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($diagnostico->versiones as $version)
                                                @php
                                                    $data_version = json_decode($version->data);
                                                @endphp

                                                <tr data-id="{{ $version->id }}">
                                                    <td>{{ $version->id }}</td>
                                                    <td>{{ date('d/m/Y H:i', strtotime($version->fecha)) }}</td>
                                                    <td>{{ date('d/m/Y', strtotime($data_version->fecha)) }}</td>
                                                    <td>{{ Crypt::decryptString($data_version->descripcion) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
