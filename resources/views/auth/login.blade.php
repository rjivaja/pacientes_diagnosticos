@extends('layouts.guest')

@section('content')

    <div class="row justify-content-center">
        <div class="col-10 col-login my-5 ">
            <div class="card card-flat">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success mb-5 mx-5" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" id="loginForm" novalidate action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <!-- Email Address -->
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <input class="form-control border-right-0 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" type="email" value="{{ old('email') }}" placeholder="{{ __('Email') }}" required autofocus>
                                <div class="input-group-append">
                                    <span class="input-group-text text-muted bg-transparent border-left-0">
                                        <em class="fas fa-user"></em>
                                    </span>
                                </div>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!-- Password -->
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <input class="form-control border-right-0 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" type="password" placeholder="{{ __('Password') }}" required autocomplete="current-password">
                                <div class="input-group-append toggle-password">
                                    <span class="input-group-text text-muted bg-transparent border-left-0">
                                        <em class="fas fa-eye"></em>
                                    </span>
                                </div>
                                @if ($errors->has('password'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <button class="btn btn-block btn-primary mt-3" type="submit">{{ __('Log in') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
