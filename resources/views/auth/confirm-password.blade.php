@extends('layouts.guest')

@section('content')

    <div class="row justify-content-center">
        <div class="col-10 col-login my-5 ">
            <div class="card card-flat">
                <div class="card-header text-center">
                    <div class="logo">
                        <img class="block-center img-fluid" src="{{ settings('logo')->getFullUrl() }}" alt="Logo">
                    </div>
                </div>
                <div class="card-body">
                    <p class="text-center py-2">{{ __('This is a secure area of the application. Please confirm your password before continuing.') }}</p>
                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <!-- Password -->
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <input class="form-control border-right-0 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" type="password" placeholder="{{ __('Password') }}" required autocomplete="current-password">
                                <div class="input-group-append toggle-password">
                                    <span class="input-group-text text-muted bg-transparent border-left-0">
                                        <em class="fas fa-eye"></em>
                                    </span>
                                </div>
                                @if ($errors->has('password'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <button class="btn btn-block btn-primary mt-3" type="submit">{{ __('Confirm') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
