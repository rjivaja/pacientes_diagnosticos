@extends('layouts.guest')

@section('content')

    <div class="row justify-content-center">
        <div class="col-10 col-login my-5 ">
            <div class="card card-flat">
                <div class="card-header text-center">
                    <div class="logo">
                        <img class="block-center img-fluid" src="{{ settings('logo')->getFullUrl() }}" alt="Logo">
                    </div>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success mb-5 mx-5" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p class="text-center py-2">{{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}</p>
                    <form method="POST" id="loginForm" action="{{ route('password.email') }}">
                        @csrf

                        <!-- Email Address -->
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <input class="form-control border-right-0 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" type="email" value="{{ old('email') }}" placeholder="{{ __('Email') }}" required autofocus>
                                <div class="input-group-append">
                                    <span class="input-group-text text-muted bg-transparent border-left-0">
                                        <em class="fas fa-envelope"></em>
                                    </span>
                                </div>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <button class="btn btn-block btn-primary mt-3" type="submit">{{ __('Email Password Reset Link') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
