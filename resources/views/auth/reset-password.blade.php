@extends('layouts.guest')

@section('content')

    <div class="row justify-content-center">
        <div class="col-10 col-login my-5 ">
            <div class="card card-flat">
                <div class="card-header text-center">
                    <div class="logo">
                        <img class="block-center img-fluid" src="{{ settings('logo')->getFullUrl() }}" alt="Logo">
                    </div>
                </div>
                <div class="card-body">
                    <p class="text-center py-2">Inserta tu email y tu nueva contraseña</p>
                    <form method="POST" action="{{ route('password.update') }}" aria-label="{{ __('Reset Password') }}">
                        @csrf

                        <!-- Password Reset Token -->
                        <input type="hidden" name="token" value="{{ $request->route('token') }}">

                        <!-- Email Address -->
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <input class="form-control border-right-0 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" type="email" value="{{ old('email', $request->email) }}" placeholder="{{ __('Email') }}" required autofocus>
                                <div class="input-group-append">
                                    <span class="input-group-text text-muted bg-transparent border-left-0">
                                        <em class="fas fa-envelope"></em>
                                    </span>
                                </div>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!-- Password -->
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <input class="form-control border-right-0 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" type="password" placeholder="{{ __('Password') }}" required>
                                <div class="input-group-append toggle-password">
                                    <span class="input-group-text text-muted bg-transparent border-left-0">
                                        <em class="fas fa-eye"></em>
                                    </span>
                                </div>
                                @if ($errors->has('password'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <!-- Confirm Password -->
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <input class="form-control border-right-0 {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" type="password" placeholder="{{ __('Confirm Password') }}" required>
                                <div class="input-group-append toggle-password">
                                    <span class="input-group-text text-muted bg-transparent border-left-0">
                                        <em class="fas fa-eye"></em>
                                    </span>
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <button class="btn btn-block btn-submit mt-3" type="submit">{{ __('Reset Password') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
