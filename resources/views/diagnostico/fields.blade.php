<div class="row">
    <div class="col-xl-8 col-xxl-6">
        <div class="card card-default">
            <div class="card-header">
                Datos de diagnóstico
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 form-group">
                        <label>Fecha <small class="text-danger">(*)</small></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar"></i></span>
                            </div>
                            {!! Form::date('fecha', date('Y-m-d', strtotime($fecha)), ['class' => 'form-control date']) !!}
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label>Descripción<small class="text-danger">(*)</small></label>
                        {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
