@extends('layouts.app')

@section('title', 'Editar diagnóstico')

@section('content')

    {!! Form::model($diagnostico, ['route' => ['diagnosticos.update', $diagnostico->id], 'method' => 'PUT']) !!}
        @include('diagnostico.fields')

        <div class="row">
            <div class="col-xl-8 col-xxl-6 text-right">
                <button class="btn btn-secondary btn-back mr-2" type="button"><i class="fas fa-times pr-2"></i> Cancelar</button>
                <button class="btn btn-primary" type="submit"><i class="fas fa-save pr-2"></i> Editar diagnóstico</button>
            </div>
        </div>
    {!! Form::close() !!}

@endsection
