@extends('layouts.app')

@section('title', 'Nuevo diagnóstico de paciente')

@section('content')

    {!! Form::model($paciente, ['route' => ['pacientes.diagnosticos.store', $paciente->id], 'method' => 'POST']) !!}
        @include('diagnostico.fields')

        <div class="row">
            <div class="col-xl-8 col-xxl-6 text-right">
                <button class="btn btn-primary" type="submit"><i class="fas fa-save pr-2"></i> Guardar</button>
            </div>
        </div>
    {!! Form::close() !!}

@endsection
